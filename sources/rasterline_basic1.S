start:
	CIAAPRA=	$bfe001						Input/Output for pin 6 (port 1 & 2 fire buttons) http://amiga-dev.wikidot.com/information:hardware#toc3
	VHPOSR=		$dff006						Read raster XY position	http://amiga-dev.wikidot.com/hardware:vhposr
	COLOR00=	$dff180						Palette color 0 http://amiga-dev.wikidot.com/hardware:colorx
	INTENAR=	$dff01c						Interrupt enable bits read
	INTENAW=	$dff09a						Interrupt enable bits (clear/set bits) http://amiga-dev.wikidot.com/hardware:intenar
	RASTERSTART=$ff 						$02c is the top of the screen
init:
	move #RASTERSTART,d7					d7 initialized to $0ff
	move INTENAR,d1							read the Interrupt enable bits into d1
	move #$7fff,INTENAW						disable all Interrupts
mainloop:
	btst #0,VHPOSR-1						test if raster high byte is 0
	bne mainloop 							if not loop again to wait
	cmp.b #$2c,VHPOSR 						test if raster low byte is $2c (raster at top?)
	bne mainloop							if not loop again to wait
	move.w #$000,COLOR00					move color black to COLOR00(background palette)

waitraster1:
	cmp.b VHPOSR,d7							raster on our desired position?
	bne waitraster1							if not loop again to wait
	move.w #$fff,COLOR00					move color white to COLOR00(background palette) we are drawing the line
waitraster2:
	cmp.b VHPOSR,d7							raster on our desired position? this second check ensures we are at the edge of just leaving our beloved position
	beq waitraster2							if not loop again to wait
	move.w #$000,COLOR00					move color black to COLOR00(background palette) we are done drawing

	btst #6,CIAAPRA							is left mouse button clicked?
	bne  mainloop							if not start over running the main loop
exit:
	or #$c000,d1 							re-enabling all Interrupts
	move d1,INTENAW 							writing the Interrupt enable bits
	rts 									return to AmigaOS (did we forget something? I think so)
