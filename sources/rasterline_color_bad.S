;	ORG $73000
;	LOAD $73000
;	JUMPPTR init

	SECTION tutcode,CODE_C

init:
	MOUSECLICK=$bfe001
	RASTERPTRL=$dff006
	RASTERPTRH=$dff005
	RASTERLINECLR=$dff180
	INTENAR=$dff01c
	INTENAW=$dff09a
	COPPERADDR=$dff080
	COPPERRESTARTADDR=$dff088

	CPRFETCHMODE=$1fc
	CPRBITPLANECNTL=$100
	CPRRASTERLINECLR=$180

	RASTERSTART=$02d
	RASTEREND=$115

	INCREMENT=1
	BORDERTHICKNESS=4
	BARCOLORCOUNT=6

***********************************************************

	move.l 4.w,a6		; execbase
	clr.l d0
	move.l #gfxname,a1
	jsr -408(a6)		; oldopenlibrary*()
	move.l d0,a1
	move.l 38(a1),d4	; original copper ptr
	jsr -414(a6)		; closelibrary()
	

	move.w #RASTERSTART,d7
	moveq #INCREMENT,d6
	move.w INTENAR,d1	; save interrupt vector
	move.w #$7fff,INTENAW	; disable interrupts

	move.l #copper,COPPERADDR   ; get copper
	move.w #0,COPPERRESTARTADDR ; restart copper

***********************************************************

mainloop:
waitframe1:			; raster delay
	btst #0,RASTERPTRH
	bne.b waitframe1 
	cmp.b #$2b,RASTERPTRL
	bne.s waitframe1 

waitframe2:			; more raster delay
	cmp.b #$2a,RASTERPTRL
	beq.b waitframe2

	move.w #$113,RASTERLINECLR

	add d6,d7
	cmp.w #RASTEREND,d7
	blo.s .a
	neg d6
.a:
	cmp.w #RASTERSTART,d7
	bhi.s .b
	neg d6
.b:	cmp.w #$0ff,d7
	blo.s .c		; raster position $ff
	bhi.s .d		; should be handled separately
.c:
	move.b #$2f,border1
	move.b #$ff,border2
	jmp .e
.d:
	move.b #$ff,border1	
	move.b #$29,border2
.e:
	move.l #waitrastera,a0
	move.w d7,d0
	moveq #BARCOLORCOUNT-1,d1
.f:
	move.b d0,(a0)
	cmp.b #0,d1
	add.w #BORDERTHICKNESS,d0
	add.w #8,a0	
	DBF d1,.f

	btst #6,MOUSECLICK
	bne.w  mainloop

exit:
	move.l d4,COPPERADDR	; restore old copper
	or #$c000,d1		; enable default interrupts
	move.w d1,INTENAW		; restore default interrupts
	rts

gfxname:
	dc.b "graphics.library",0
 

	SECTION tutdata,DATA_C		; custom chip data should be in chipM
;	EVEN

copper:
	dc.w CPRFETCHMODE,0		; slow fetch node, AGA compatibility
	dc.w CPRBITPLANECNTL,$0200	; disable bitplanes

	dc.w CPRRASTERLINECLR,$349
	dc.w $2d01,$ff00
	dc.w CPRRASTERLINECLR,$56c
	dc.w $2e01,$ff00
	dc.w CPRRASTERLINECLR,$113
border1:
	dc.w $00df,$fffe

waitrastera:
	dc.w $0001,$ff00
	dc.w CPRRASTERLINECLR,$055
;	dc.w $0001,$ff00
waitrasterb:
	dc.w $0001,$ff00
	dc.w CPRRASTERLINECLR,$0aa
;	dc.w $0001,$ff00
waitrasterc:
	dc.w $0001,$ff00
	dc.w CPRRASTERLINECLR,$0ff
;	dc.w $00d1,$ff00
waitrasterd:
	dc.w $0001,$ff00
	dc.w CPRRASTERLINECLR,$0aa
;	dc.w $0001,$ff00
waitrastere:
	dc.w $0001,$ff00
	dc.w CPRRASTERLINECLR,$055
;	dc.w $0001,$ff00
waitrasterf:
	dc.w $0001,$ff00
	dc.w CPRRASTERLINECLR,$113
;	dc.w $0001,$ff00

border2:
	dc.w $00df,$fffe

	dc.w $2a01,$ff00
	dc.w CPRRASTERLINECLR,$56c
	dc.w $2b01,$ff00
	dc.w CPRRASTERLINECLR,$113
	dc.w $2d01,$ff00
	dc.w CPRRASTERLINECLR,$349
	dc.w $ffff,$fffe
